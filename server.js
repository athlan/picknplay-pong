var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server)
  , clientDispatcher = require('./src/clientDispatcher')
  , gameServer = require('./src/game/pong/gameserver.js');

server.listen(8080);

/*app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});*/

var gameserverPong = new gameServer.gameserver(app, server, io);
var clientDispatcher = new clientDispatcher.clientDispatcher(app, server, io, gameserverPong);

io.set('log level', 1); // reduce logging
io.sockets.on('connection', function (socket) {
	clientDispatcher.registerClient(socket);
});
