function PicknplayGamePong(serverAddress) {
	
	this.serverAddress = serverAddress;
	
	this.socket = null;
	this.layerRemote = null;
	this.youAreInGame = false;
	
	this.windowX = 0;
	this.windowY = 0;
	this.previousPositionX = null;
	this.previousPositionY = null;
	
	this.initAssertDependencies = function() {
		if(typeof $ === 'undefined' || typeof jQuery === 'undefined') {
			console.log('jQuery is not loaded');
			return false;
		}
		
		if(typeof io === 'undefined' || typeof io.version === 'undefined') {
			console.log('socket.io is not loaded');
			return false;
		}
		
		return true;
	}
	
	this.indicateWindowSize = function() {
		this.windowX = $(window).width();
		this.windowY = $(window).height();
	}
	
	this.init = function() {
		if(!this.initAssertDependencies())
			return;
		
		that = this;
		
		this.indicateWindowSize();
		this.displayInitLayer();
		this.displayScreenStart();
		
		$(document).on('vmousemove', this.scrollPreventListener);
		$(window).on('orientationchange', this.indicateWindowSize);
	};
	
	this.displayInitLayer = function() {
		var remote = $('<div class="gamePongRemotePanel"><table><tr><td></td></tr></table></div>');
		$('body').prepend(remote);
		this.layerRemote = remote.find('td');
	};
	
	this.getLayerMessage = function(autoCreate) {
		var layer = $('.message', this.layerRemote);
		
		if(layer.length == 0 && autoCreate === true) {
			this.layerRemote.append('<div class="message"></div>');
			layer = $('.message', this.layerRemote);
		}
		
		return layer;
	};
	
	this.removeLayerMessage = function() {
		$('.message', this.layerRemote).remove();
	};
	
	var queueNumber = 0;
	
	this.displayScreenStart = function() {
		var layerMessage = this.getLayerMessage(true);
		
		layerMessage.append('<p>Lets play now !!</p>');
		layerMessage.append('<a class="start myButton" href="#">Start!</a>');
		
		var that = this;
		$('.start', layerMessage).click(function() {
			setTimeout(function() {
				$(this).animate({ opacity: 0.5 }, 0).html('wait ...');
			}, 100);
			
			that.socket = that.socketConnect(that.socket, that.serverAddress);
			
		 	that.socket.on('signalCurrentServerState', function (data) {
				// ignore status signals when in game
				if(that.youAreInGame)
					return;
				
				if(data.isGame) {
					if(0 == queueNumber)
						queueNumber = data.count;
					else
						--queueNumber;
					
					if(queueNumber <= 0)
						return; // we will be connected on the while
					
					if(queueNumber == 1 || queueNumber == 2) {
						$('.message', that.remoteLayer).html('<p>You are the first in queue. Stay ready !!</p>');
					}
					else {
						$('.message', that.remoteLayer).html('<p>You are in queue with ' + (queueNumber - 1) + ' another users</p>');
					}
				}
				else {
					$('.message', that.remoteLayer).html('<p>Waiting for a partner...</p>');
				}
			});
			
			that.socket.on('signalWaitingForPartner', function() {
				that.youAreInGame = true;
				$('.message', that.remoteLayer).html('<p>Waiting for a partner...</p>');
			});
			
			that.socket.on('my_session_id', function(data) {

			});

			that.socket.on('welcome', function(data) {
				that.layerGameControllerDisplay();
				that.youAreInGame = true;
			});
			
			that.socket.on('gameOver', function(data) {
				if(data.score > data.scoreEnemy)
					alert('You won!');
				if(data.score < data.scoreEnemy)
					alert('You lost!');
				if(data.score == data.scoreEnemy)
					alert('Remis!');
			});

			that.socket.on('disconnect', function() {
				alert('game end!');
				// $('.connectionState').removeClass('active');
				that.layerGameControllerDestroy();
				that.youAreInGame = false;
				
				that.displayScreenStart();
				
				// close it gracefully!
				that.socketDisconnect(that.socket);
				that.socket.removeAllListeners();
			});

			that.socket.on('connect', function() {
				$('.connectionState').addClass('active');
			});
		});
	};
	
	this.layerGameControllerDisplay = function() {
		var gameController = $('<div class="gameController"></div>');
		this.layerRemote.append(gameController);
		
		gameController.append('<div class="arrow up"></div>');
		gameController.append('<div class="arrow down"></div>');
		this.getLayerMessage(true).html('<p>Move up and down</p>');
		
		pulsate($('.arrow.up', this.layerRemote), 625, 'swing', { opacity: 0 }, { opacity: 1 }, function() { return false; });
		pulsate($('.arrow.down', this.layerRemote), 625, 'swing', { opacity: 0 }, { opacity: 1 }, function() { return false; });
		
		// register listeners
		$(document).off('vmousemove', this.scrollPreventListener);
		$(document).on('vmousemove', this.layerGameControllerEventsMoveTouch);
	};
	
	this.layerGameControllerDestroy = function() {
		$('.gameController', this.layerRemote).remove();
		this.removeLayerMessage();
		
		$(document).off('vmousemove', this.layerGameControllerEventsMoveTouch);
		$(document).on('vmousemove', this.scrollPreventListener);
	};
	
	this.layerGameControllerEventsMoveTouch = function(e) {
		var x = e.pageX / that.windowX;
		var y = e.pageY / that.windowY;
		
		// var minDelta = 0.05;
		var minDelta = 0;
		
		if(that.previousPositionX === null || Math.abs(x - that.previousPositionX) > minDelta
			|| that.previousPositionY === null || Math.abs(y - that.previousPositionY) > minDelta) {
			that.previousPositionX = Math.ceil(x);
			that.previousPositionY = Math.ceil(y);
			
			that.getLayerMessage(true).html('<p>X ' + x + '; Y ' + y + '</p>');
			
			that.socket.emit('paddleMove', {
				positionX: x,
				positionY: y
			});
		}
		
		e.preventDefault();
	}
	
	this.scrollPreventListener = function(e) {
		e.preventDefault();
	}
	
	this.socketConnect = function(socket, endpoint) {
		var socket;
		
		if(!socket) {
			socket = io.connect(endpoint, {
				reconnect: false
			});
		} else {
			socket.removeAllListeners();
			socket.disconnect();
			socket.socket.connect(endpoint);
		}

		return socket;
	};
	
	this.socketDisconnect = function(socket) {
		if(socket) {
			socket.disconnect();
			socket.removeAllListeners();
		}
	};
}
