function hackDisablePageScrollingOnTouch() {
	document.ontouchmove = function(e){ 
	    e.preventDefault(); 
	}
}

function hackHideBrowserAddressBar() {
	$(window).on('orientationchange', function () {
		setTimeout(function() { window.scrollTo(0, 1); }, 0);
	});
	$(function () {
		var layer = $('.gamePongRemotePanel');
		layer.height(parseInt(layer.height()) + 100);
		setTimeout(function() { window.scrollTo(0, 1); }, 0);
	});
}

function pulsate(object, duration, easing, properties_to, properties_from, until_callbcak) {
	object.animate(properties_to, duration, easing, function() {
		if (until_callbcak() == false)       
			pulsate(object, duration, easing, properties_from, properties_to, until_callbcak);
	});
}
