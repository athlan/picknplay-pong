function GameSession() {
	this.players = [];
	
	this.onSessionClose = function() {};
}

exports.clientDispatcher = function ClientDispatcher(app, server, io, gameserver) {
	
	this.gamersQueue = [];
	this.currentGame = null;
	
	var that = this;
	
	this.registerClient = function(socket) {
		
		// add user to queue
		this.gamersQueue.push(socket);
		
		this.tryDispatchGame();
		io.sockets.emit('signalCurrentServerState', this.getServerCurrentState());
		
		// unregister the gamer
		socket.on('disconnect', function() {
			var index = that.gamersQueue.indexOf(socket);
			if(index > -1) {
				that.gamersQueue.splice(that.gamersQueue.indexOf(socket), 1);
			}
			
			io.sockets.emit('signalCurrentServerState', that.getServerCurrentState());
		});

		socket.on('commandDisconnect', function() {
			socket.disconnect();
		});
	}
	
	this.getServerCurrentState = function() {
		return {
			count: that.gamersQueue.length,
			isGame: (that.currentGame !== null)
		};
	}
	
	this.tryDispatchGame = function() {
		this.debugServerState('trying dispatch game');
		
		if(this.currentGame !== null)
			return false;

		if(this.gamersQueue.length < 2) {
			for(var i = 0; i < this.gamersQueue.length; ++i) {
				io.sockets.socket(this.gamersQueue[i].id).emit('signalWaitingForPartner', { a: 1 });
			}
			
			return false;
		}
		
		// serverStateDebug('pre game started');
		
		var gameSession = new GameSession();
		
		gameSession.onSessionClose = function() {
			that.currentGame = null;
			that.tryDispatchGame(app, server, io);
		}
		
		this.currentGame = gameSession;
		
		gameSession.players.push(this.gamersQueue.shift());
		gameSession.players.push(this.gamersQueue.shift());
		
		this.debugServerState('on game started');
		gameserver.gameRun(gameSession);
	}
	
	this.debugServerState = function(contextMessage) {
		console.log('----');
		console.log(contextMessage);
		console.log('gamersQueue:', this.gamersQueue.length);
		console.log('game:', this.currentGame !== null);
	}
}
