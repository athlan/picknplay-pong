var gameserver_app = require('express')()
  , gameserver_server = require('http').createServer(gameserver_app)
  , gameserver_io = require('socket.io').listen(gameserver_server);

gameserver_server.listen(8081);

gameserver_io.set('log level', 1); // reduce logging
gameserver_io.sockets.on('connection', function (socket) {
	
	socket.on('signalGameStop', function(data) {
		for(var i = 0; i < currentGames.length; ++i) {
			currentGames[i].gameStop(data);
		}
	});
	
});

currentGames = [];

exports.gameserver = function(app, server, io) {
	
	// register game
	currentGames.push(this);
	
	this.currentSession = null;
	
	this.gameStopTriggered = false;
	
	var that = this;
	
	this.eventHandlerPaddleMove = function(data) {
		console.log(data);
		gameserver_io.sockets.emit('paddleMove', data);
	}
	
	this.gameRun = function(gameSession) {
		this.currentSession = gameSession;
		this.gameStopTriggered = false;
		
		io.sockets.socket(gameSession.players[0].id).emit('welcome', {a: 1});
		io.sockets.socket(gameSession.players[1].id).emit('welcome', {a: 1});
		
		gameSession.players[0].on('paddleMove', function(data) {
			data.paddleId = 'l';
			that.eventHandlerPaddleMove(data);
		});
		gameSession.players[1].on('paddleMove', function(data) {
			data.paddleId = 'r';
			that.eventHandlerPaddleMove(data);
		});
		
		gameserver_io.sockets.emit('signalGameStart', {});
		
		// TODO: when the game is over than request for new disacthGame.
		// setTimeout(function() {
		// 			that.gameStop(gameSession);
		// 		}, 8000);
		
		gameSession.players[0].on('disconnect', function() {
			that.gameStop();
		});
		gameSession.players[1].on('disconnect', function() {
			that.gameStop();
		});
	}
	
	this.gameStop = function(data) {
		if(this.gameStopTriggered === true)
			return;
		
		this.gameStopTriggered = true;
		
		// send final resuts and exit reasin
		// if possible.
		
		// unregister players.
		var dataStop = {
			score: 0,
			scoreEnemy: 0
		};
		
		this.currentSession.players[0].emit('gameOver', {
			score: (typeof data != 'undefined' && typeof data.scoreL != 'undefined') ? data.scoreL : 0,
			scoreEnemy: (typeof data != 'undefined' && typeof data.scoreR != 'undefined') ? data.scoreR : 0,
		});
		this.currentSession.players[1].emit('gameOver', {
			score: (typeof data != 'undefined' && typeof data.scoreR != 'undefined') ? data.scoreR : 0,
			scoreEnemy: (typeof data != 'undefined' && typeof data.scoreL != 'undefined') ? data.scoreL : 0,
		});
		
		this.currentSession.players[0].disconnect();
		this.currentSession.players[1].disconnect();
		
		// callback session
		if(this.currentSession !== null) {
			this.currentSession.onSessionClose();
			this.currentSession = null;
		}
		
		gameserver_io.sockets.emit('signalGameStop', {});
	}
}
