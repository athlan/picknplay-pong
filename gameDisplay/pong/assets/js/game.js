function PongBall() {
	
	this.game = null;
	
	this.a = {
		x: 0,
		y: 0
	}
	
	this.position = {
		x: 0,
		y: 0
	}
	
	this.dim = {
		x: 100,
		y: 100
	}
	
	this.obj = null;
	this.r = 0;
	
	this.init  = function(game) {
		this.game = game;
		
		this.obj = $('<div class="ball"></div>');
		$('.game').append(this.obj);
		
		this.initPositionCenter();
	}
	
	this.initPositionCenter = function() {
		this.position.x = Math.ceil((this.game.dim.x - this.dim.x) / 2);
		this.position.y = Math.ceil((this.game.dim.y - this.dim.y) / 2);
		this.redraw();
	}
	
	this.redraw = function() {
		this.obj.css('top', this.position.y + 'px');
		this.obj.css('left', this.position.x + 'px');
	}
	
	this.update = function() {
		this.position.x += this.a.x;
		this.position.y += this.a.y;
		
		this.redraw();
	}
}

function PongPaddle() {
	this.game = null;
	this.obj = null;
	
	this.dim = {
		x: 50,
		y: 100
	}
	
	this.position = {
		y: 100
	}
	
	this.init = function(game, obj) {
		this.game = game;
		this.obj = obj;
		
		this.initPositionCenter();
	}
	
	this.initPositionCenter = function() {
		this.position.y = Math.ceil((this.game.dim.y - this.dim.y) / 2);
		this.redraw();
	}
	
	this.redraw = function() {
		this.obj.css('top', this.position.y + 'px');
	}
}

function Pong() {
	
	this.socket = null;
	this.gameInteval = null;
	
	this.dim = {
		x: 0,
		y: 0
	};
	
	this.paddleL = null;
	this.paddleR = null;
	
	this.scoreL = 0;
	this.scoreR = 0;
	this.scoreRemaining = 5;
	
	this.balls = [];
	
	var that = this;
	
	this.init = function(socket) {
		this.socket = socket;
		this.initSockets();
		
		this.updateWindowsDimensions();
		$(window).on('resize', this.updateWindowsDimensions);
		
		var b = new PongBall();
		b.init(this);
		b.a.x = 3;
		b.a.y = 1;
		this.balls.push(b);
		
		this.paddleL = new PongPaddle();
		this.paddleL.init(this, $('.paddle.L'));
		
		this.paddleR = new PongPaddle();
		this.paddleR.init(this, $('.paddle.R'));
		
		this.scoreRedraw();
		
		/*$(document).on('mouseover', function() {
			that.gameplayResume();
		});*/
		$(document).on('paddleMove', function() {
			that.gameplayResume();
		});
	}
	
	this.initSockets = function() {
		this.socket.on('connect', function() {
			activateScreen('splash')
		});
		
		this.socket.on('signalGameStart', function() {
			that.gameplayNew();
			that.gameplayReset();
			that.gameplayResume();
			
			activateScreen('game')
		});
		
		this.socket.on('signalGameStop', function() {
			that.gameplayPause();
			that.gameplayReset();
			
			activateScreen('splash')
		});
		
		this.socket.on('paddleMove', function(data) {
			var paddle = null;
			
			if(data.paddleId == 'l') {
				paddle = that.paddleL;
			}
			else if(data.paddleId == 'r') {
				paddle = that.paddleR;
			}
			
			paddle.position.y = data.positionY * that.dim.y;
			paddle.redraw();
			
			$(document).trigger('paddleMove');
			
		});
		
		$('.exitgames').on('click', function() {
			that.socket.emit('signalGameStop');
		});
	}
	
	this.updateWindowsDimensions = function() {
		var w = $(window);
		
		this.dim.x = w.width();
		this.dim.y = w.height();
	}
	
	this.update = function() {
		for (var i = 0; i < this.balls.length; ++i) {
			var ball = this.balls[i];
			ball.update();
			
			if(ball.position.x + ball.dim.x >= this.dim.x - this.paddleR.dim.x) {
				// right paddle
				var collisionPoint = ball.position.y + ball.dim.y/2;
				if(collisionPoint < this.paddleR.position.y || collisionPoint > this.paddleR.position.y + this.paddleR.dim.y) {
					this.scoreGoal('L');
				}
				
				ball.a.x *= -1;
			}
			if(ball.position.x <= this.paddleL.dim.x) {
				// left paddle
				var collisionPoint = ball.position.y + ball.dim.y/2;
				if(collisionPoint < this.paddleL.position.y || collisionPoint > this.paddleL.position.y + this.paddleL.dim.y) {
					this.scoreGoal('R');
				}
				
				ball.a.x *= -1;
			}
			
			if(ball.position.y + ball.dim.y >= this.dim.y
				|| ball.position.y <= 0)
				ball.a.y *= -1;
		}
	}
	
	this.scoreRedraw = function() {
		$('.points.L').html(this.scoreL);
		$('.points.R').html(this.scoreR);
		$('.pointsRemaining > span').html(this.scoreRemaining);
	}
	
	this.gameplayNew = function() {
		that.scoreRemaining = 5;
		that.scoreL = 0;
		that.scoreR = 0;
		
		that.gameplayPause();
	}
	
	this.gameplayResume = function() {
		if(this.gameInteval !== null || 0 >= that.scoreRemaining)
			return;
		
		this.gameInteval = setInterval(function() {
			that.update();
		}, 10);
	}
	
	this.gameplayPause = function() {
		if(null !== this.gameInteval)
			clearInterval(this.gameInteval);
		
		this.gameInteval = null;
	}
	
	this.gameplayReset = function() {
		for (var i = 0; i < this.balls.length; ++i) {
			var ball = this.balls[i];
			ball.initPositionCenter();
		}
	}
	
	this.scoreGoal = function(mode) {
		this.gameplayPause();
		this.gameplayReset();
		
		if(mode == 'L') {
			++this.scoreL;
		}
		if(mode == 'R') {
			++this.scoreR;
		}
		
		--this.scoreRemaining;
		
		if(0 == this.scoreRemaining) {
			that.socket.emit('signalGameStop', {
				scoreL: this.scoreL,
				scoreR: this.scoreR
			});
		}
		
		this.scoreRedraw();
	}
}

function activateScreen(name) {
	$('.screen').removeClass('active')
	$('.screen.' + name).addClass('active')
}
